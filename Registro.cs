﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Veterinaria
{
    public partial class Registro : Form
    {
        SqlConnection cn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Lasek\UNEDL\UNEDL2020B\4to\Windows\Veterinaria\Database1.mdf;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;

        public Registro()
        {
            InitializeComponent();
        }

        private void Registro_Load(object sender, EventArgs e)
        {
            cmd.Connection = cn;
            cargarLista();
        }

        private void buttonAnadir_Click(object sender, EventArgs e)
        {
            if (textBoxID.Text != "" & textBoxNombre.Text != "" & textBoxTelefono.Text != "" & textBoxNombreMascota.Text != "" & textBoxRaza.Text != "")
            {
                cn.Open();
                cmd.CommandText = "insert into info (id, nombre, telefono, nombremascota, raza) values ('"+textBoxID.Text+"','"+textBoxNombre.Text+ "','" + textBoxTelefono.Text + "','" + textBoxNombreMascota.Text + "','" + textBoxRaza.Text + "')";
                cmd.ExecuteNonQuery();
                cmd.Clone();
                MessageBox.Show("Se ha guardado el registro.", "Veterinaria");
                cn.Close();
                textBoxID.Text = "";
                textBoxNombre.Text = "";
                textBoxTelefono.Text = "";
                textBoxNombreMascota.Text = "";
                textBoxRaza.Text = "";
                cargarLista();
            }
        }
        private void cargarLista()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            cn.Open();
            cmd.CommandText = "select * from info";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    listBox1.Items.Add(dr[0].ToString());
                    listBox2.Items.Add(dr[1].ToString());
                    listBox3.Items.Add(dr[2].ToString());
                    listBox4.Items.Add(dr[3].ToString());
                    listBox5.Items.Add(dr[4].ToString());
                }
            }
            cn.Close();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox l = sender as ListBox;
            if(l.SelectedIndex != -1)
            {
                listBox1.SelectedIndex = l.SelectedIndex;
                listBox2.SelectedIndex = l.SelectedIndex;
                listBox3.SelectedIndex = l.SelectedIndex;
                listBox4.SelectedIndex = l.SelectedIndex;
                listBox5.SelectedIndex = l.SelectedIndex;
                textBoxID.Text = listBox1.SelectedItem.ToString();
                textBoxNombre.Text = listBox2.SelectedItem.ToString();
                textBoxTelefono.Text = listBox3.SelectedItem.ToString();
                textBoxNombreMascota.Text = listBox4.SelectedItem.ToString();
                textBoxRaza.Text = listBox5.SelectedItem.ToString();
            }
        }

        private void buttonBorrar_Click(object sender, EventArgs e)
        {
            if (textBoxID.Text != "" & textBoxNombre.Text != "" & textBoxTelefono.Text != "" & textBoxNombreMascota.Text != "" & textBoxRaza.Text != "")
            {
                cn.Open();
                cmd.CommandText = "delete from info where id='"+textBoxID.Text+"'and nombre='"+textBoxNombre.Text+"'" +
                    "and telefono='" + textBoxTelefono.Text + "'and nombremascota='" + textBoxNombreMascota.Text + "' and raza='" + textBoxRaza.Text + "'";
                cmd.ExecuteNonQuery();
                cn.Close();
                MessageBox.Show("Registro borrado.", "Veterinaria");
                cargarLista();
                textBoxID.Text = "";
                textBoxNombre.Text = "";
                textBoxTelefono.Text = "";
                textBoxNombreMascota.Text = "";
                textBoxRaza.Text = "";
            }
        }

        private void buttonActualizar_Click(object sender, EventArgs e)
        {
            if (textBoxID.Text != "" & textBoxNombre.Text != "" & textBoxTelefono.Text != "" & textBoxNombreMascota.Text != "" & textBoxRaza.Text != "" & listBox1.SelectedIndex != -1)
            {
                cn.Open();
                cmd.CommandText = "update info set id='" + textBoxID.Text + "' ,nombre='" + textBoxNombre.Text + "'" +
                    " ,telefono='" + textBoxTelefono.Text + "' ,nombremascota='" + textBoxNombreMascota.Text + "' ,raza='" + textBoxRaza.Text + "'" +
                    " where id='" + listBox1.SelectedItem.ToString() + "' and nombre='" + listBox2.SelectedItem.ToString() +
                    "' and telefono='" + listBox3.SelectedItem.ToString() + "' and nombremascota='" + listBox4.SelectedItem.ToString() + "' and raza='" + listBox5.SelectedItem.ToString() +"'";
                cmd.ExecuteNonQuery();
                cn.Close();
                MessageBox.Show("Registro actualizado.", "Veterinaria");
                cargarLista();
                textBoxID.Text = "";
                textBoxNombre.Text = "";
                textBoxTelefono.Text = "";
                textBoxNombreMascota.Text = "";
                textBoxRaza.Text = "";
            }
        }
    }
}