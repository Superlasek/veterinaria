﻿namespace Veterinaria
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelBarraTitulo = new System.Windows.Forms.Panel();
            this.ButtonRestaurar = new System.Windows.Forms.PictureBox();
            this.ButtonMinimizar = new System.Windows.Forms.PictureBox();
            this.ButtonMaximizar = new System.Windows.Forms.PictureBox();
            this.ButtonCerrar = new System.Windows.Forms.PictureBox();
            this.panelMenuHorizontal = new System.Windows.Forms.Panel();
            this.buttonRegistro = new System.Windows.Forms.Button();
            this.buttonInicio = new System.Windows.Forms.Button();
            this.ButtonLogoInicio = new System.Windows.Forms.PictureBox();
            this.panelContenedor = new System.Windows.Forms.Panel();
            this.panelBarraTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRestaurar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonMaximizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCerrar)).BeginInit();
            this.panelMenuHorizontal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonLogoInicio)).BeginInit();
            this.SuspendLayout();
            // 
            // panelBarraTitulo
            // 
            this.panelBarraTitulo.BackColor = System.Drawing.Color.Coral;
            this.panelBarraTitulo.Controls.Add(this.ButtonRestaurar);
            this.panelBarraTitulo.Controls.Add(this.ButtonMinimizar);
            this.panelBarraTitulo.Controls.Add(this.ButtonMaximizar);
            this.panelBarraTitulo.Controls.Add(this.ButtonCerrar);
            this.panelBarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBarraTitulo.Location = new System.Drawing.Point(0, 0);
            this.panelBarraTitulo.Name = "panelBarraTitulo";
            this.panelBarraTitulo.Size = new System.Drawing.Size(1300, 35);
            this.panelBarraTitulo.TabIndex = 0;
            this.panelBarraTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelBarraTitulo_MouseDown);
            // 
            // ButtonRestaurar
            // 
            this.ButtonRestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonRestaurar.Image = ((System.Drawing.Image)(resources.GetObject("ButtonRestaurar.Image")));
            this.ButtonRestaurar.Location = new System.Drawing.Point(1226, 3);
            this.ButtonRestaurar.Name = "ButtonRestaurar";
            this.ButtonRestaurar.Size = new System.Drawing.Size(28, 29);
            this.ButtonRestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ButtonRestaurar.TabIndex = 0;
            this.ButtonRestaurar.TabStop = false;
            this.ButtonRestaurar.Visible = false;
            this.ButtonRestaurar.Click += new System.EventHandler(this.ButtonRestaurar_Click);
            // 
            // ButtonMinimizar
            // 
            this.ButtonMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMinimizar.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMinimizar.Image")));
            this.ButtonMinimizar.Location = new System.Drawing.Point(1192, 3);
            this.ButtonMinimizar.Name = "ButtonMinimizar";
            this.ButtonMinimizar.Size = new System.Drawing.Size(28, 29);
            this.ButtonMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ButtonMinimizar.TabIndex = 0;
            this.ButtonMinimizar.TabStop = false;
            this.ButtonMinimizar.Click += new System.EventHandler(this.ButtonMinimizar_Click);
            // 
            // ButtonMaximizar
            // 
            this.ButtonMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMaximizar.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMaximizar.Image")));
            this.ButtonMaximizar.Location = new System.Drawing.Point(1226, 3);
            this.ButtonMaximizar.Name = "ButtonMaximizar";
            this.ButtonMaximizar.Size = new System.Drawing.Size(28, 29);
            this.ButtonMaximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ButtonMaximizar.TabIndex = 0;
            this.ButtonMaximizar.TabStop = false;
            this.ButtonMaximizar.Click += new System.EventHandler(this.ButtonMaximizar_Click_1);
            // 
            // ButtonCerrar
            // 
            this.ButtonCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonCerrar.Image = ((System.Drawing.Image)(resources.GetObject("ButtonCerrar.Image")));
            this.ButtonCerrar.Location = new System.Drawing.Point(1260, 3);
            this.ButtonCerrar.Name = "ButtonCerrar";
            this.ButtonCerrar.Size = new System.Drawing.Size(28, 29);
            this.ButtonCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ButtonCerrar.TabIndex = 0;
            this.ButtonCerrar.TabStop = false;
            this.ButtonCerrar.Click += new System.EventHandler(this.ButtonCerrar_Click);
            // 
            // panelMenuHorizontal
            // 
            this.panelMenuHorizontal.BackColor = System.Drawing.Color.Bisque;
            this.panelMenuHorizontal.Controls.Add(this.buttonRegistro);
            this.panelMenuHorizontal.Controls.Add(this.buttonInicio);
            this.panelMenuHorizontal.Controls.Add(this.ButtonLogoInicio);
            this.panelMenuHorizontal.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelMenuHorizontal.Location = new System.Drawing.Point(1160, 35);
            this.panelMenuHorizontal.Name = "panelMenuHorizontal";
            this.panelMenuHorizontal.Size = new System.Drawing.Size(140, 615);
            this.panelMenuHorizontal.TabIndex = 1;
            // 
            // buttonRegistro
            // 
            this.buttonRegistro.BackColor = System.Drawing.Color.SeaShell;
            this.buttonRegistro.FlatAppearance.BorderColor = System.Drawing.Color.Coral;
            this.buttonRegistro.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSalmon;
            this.buttonRegistro.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonRegistro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRegistro.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegistro.Location = new System.Drawing.Point(24, 239);
            this.buttonRegistro.Name = "buttonRegistro";
            this.buttonRegistro.Size = new System.Drawing.Size(100, 40);
            this.buttonRegistro.TabIndex = 0;
            this.buttonRegistro.Text = "Registro";
            this.buttonRegistro.UseVisualStyleBackColor = false;
            this.buttonRegistro.Click += new System.EventHandler(this.buttonRegistro_Click);
            // 
            // buttonInicio
            // 
            this.buttonInicio.BackColor = System.Drawing.Color.SeaShell;
            this.buttonInicio.FlatAppearance.BorderColor = System.Drawing.Color.Coral;
            this.buttonInicio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSalmon;
            this.buttonInicio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInicio.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInicio.Location = new System.Drawing.Point(24, 164);
            this.buttonInicio.Name = "buttonInicio";
            this.buttonInicio.Size = new System.Drawing.Size(100, 40);
            this.buttonInicio.TabIndex = 0;
            this.buttonInicio.Text = "Inicio";
            this.buttonInicio.UseVisualStyleBackColor = false;
            this.buttonInicio.Click += new System.EventHandler(this.buttonInicio_Click);
            // 
            // ButtonLogoInicio
            // 
            this.ButtonLogoInicio.Image = ((System.Drawing.Image)(resources.GetObject("ButtonLogoInicio.Image")));
            this.ButtonLogoInicio.Location = new System.Drawing.Point(24, 16);
            this.ButtonLogoInicio.Name = "ButtonLogoInicio";
            this.ButtonLogoInicio.Size = new System.Drawing.Size(100, 85);
            this.ButtonLogoInicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ButtonLogoInicio.TabIndex = 0;
            this.ButtonLogoInicio.TabStop = false;
            this.ButtonLogoInicio.Click += new System.EventHandler(this.ButtonLogoInicio_Click);
            // 
            // panelContenedor
            // 
            this.panelContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenedor.Location = new System.Drawing.Point(0, 35);
            this.panelContenedor.Name = "panelContenedor";
            this.panelContenedor.Size = new System.Drawing.Size(1160, 615);
            this.panelContenedor.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1300, 650);
            this.Controls.Add(this.panelContenedor);
            this.Controls.Add(this.panelMenuHorizontal);
            this.Controls.Add(this.panelBarraTitulo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panelBarraTitulo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRestaurar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonMaximizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCerrar)).EndInit();
            this.panelMenuHorizontal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonLogoInicio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelBarraTitulo;
        private System.Windows.Forms.Panel panelMenuHorizontal;
        private System.Windows.Forms.Panel panelContenedor;
        private System.Windows.Forms.PictureBox ButtonRestaurar;
        private System.Windows.Forms.PictureBox ButtonMinimizar;
        private System.Windows.Forms.PictureBox ButtonMaximizar;
        private System.Windows.Forms.PictureBox ButtonCerrar;
        private System.Windows.Forms.PictureBox ButtonLogoInicio;
        private System.Windows.Forms.Button buttonInicio;
        private System.Windows.Forms.Button buttonRegistro;
    }
}

